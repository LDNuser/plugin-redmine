Redmine::Plugin.register :test do
  name 'Test plugin'
  author 'Dmitriy Ledenev'
  description 'This is best a plugin for Redmine'
  version '0.0.1'

  menu :project_menu, :examples, { controller: 'examples', action: 'index' }, caption: 'Examples', after: :activity, param: :project_id
  delete_menu_item :project_menu, :news

  project_module :examples do
    permission :view_examples, examples: :index
    permission :vote_examples, examples: :vote
  end

  require_dependency 'examples_hook_listener'

  settings default: {'empty' => true},  partial: 'settings/example_settings'
end
