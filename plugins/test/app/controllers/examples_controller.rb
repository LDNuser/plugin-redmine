class ExamplesController < ApplicationController
  before_action :find_project, :authorize, only: :index

  def index
    @project = Project.find(params[:project_id])
    @examples = Example.all
  end

  def vote
    example = Example.find(params[:id])
    example.vote(params[:answer])
    if example.save
      flash[:notice] = 'Vote saved.'
    end
    redirect_to examples_path(project_id: params[:project_id])
  end

  private

  def find_project
    @project = Project.find(params[:project_id])
  end
end
