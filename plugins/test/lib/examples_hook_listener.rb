class ExamplesHookListener < Redmine::Hook::ViewListener
  render_on :view_projects_show_left, partial: "examples/project_overview"
end